#!/bin/bash
wget https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
zcat < install-tl-unx.tar.gz | tar xf -
rm install-tl-unx.tar.gz
cd install-tl*
mkdir /home/candidat/texlive
perl ./install-tl --no-interaction --scheme=scheme-basic --no-doc-install --no-src-install --texdir=/home/candidat/texlive
export PATH=/home/candidat/texlive/bin/x86_64-linux:$PATH
tlmgr install multido pgf beamer

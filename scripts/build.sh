#!/bin/bash

# cloud-localds (from cloud-image-utils in Debian) creates a disk image (seed.img) for cloud-init
cloud-localds seed.img cloud-config.yaml
# Download Ubuntu 22.04 LTS (jammy)
wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
qemu-img resize jammy-server-cloudimg-amd64.img +14G
# Boot the Ubuntu cloud image with qemu passing the cloud-init disk image to configure it 
qemu-system-x86_64 -m 8192 -smp 4 -net nic -net user -hda jammy-server-cloudimg-amd64.img -hdb seed.img -nographic
# convert the qemu format to VirtualBox format
qemu-img convert jammy-server-cloudimg-amd64.img jammy-server-cloudimg-amd64.raw
VBoxManage convertdd jammy-server-cloudimg-amd64.raw jammy-server-cloudimg-amd64.vdi --format VDI
